<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
 <link rel="icon" href="static/images/favicon.ico" type="image/x-icon" />
<title>WebIM</title>
<link rel="stylesheet" href="static/css/layui.css">
<link rel="stylesheet" href="static/css/global.css">
<link rel="stylesheet" href="static/css/contextMenu.css">
    <link rel="stylesheet" href="static/css/cropper.min.css">
    <link rel="stylesheet" href="static/css/ImgCropping.css">
</head>
<script src="static/layui.js"></script>
<script src="static/js/jquery-2.1.1.js"></script>
<script src="static/js/cropper.min.js"></script>
<script src="static/js/contextMenu.js"></script>
<script src="static/js/paho-mqtt.js"></script>
<body >
<%
if(session.getAttribute("user")==null){
%>
 <script type="text/javascript"> 
 window.location.href = "index";
 </script>  
<%
}
%>

<!--图片裁剪框 start-->
<div style="display: none" class="tailoring-container">
    <div class="black-cloth" onclick="closeTailor(this)"></div>
    <div class="tailoring-content">
            <div class="tailoring-content-one">
                <label title="上传图片" for="chooseImg" class="l-btn choose-btn">
                    <input type="file" accept="image/jpg,image/jpeg,image/png" name="file" id="chooseImg" class="hidden" onchange="selectImg(this)">
                    选择头像
                </label>
                <div class="close-tailoring"  onclick="closeTailor(this)">×</div>
            </div>
            <div class="tailoring-content-two">
                <div class="tailoring-box-parcel">
                    <img id="tailoringImg">
                </div>
                <div class="preview-box-parcel">
                    <p>头像预览：</p>
                    <div class="square previewImg"></div>
                    <div class="circular previewImg"></div>
                </div>
            </div>
            <div class="tailoring-content-three">
                <button class="l-btn cropper-reset-btn">还原</button>
                <button class="l-btn cropper-rotate-btn">旋转</button>
                <button class="l-btn cropper-scaleX-btn">换向</button>
                <button class="l-btn sureCut" id="sureCut">确定</button>
            </div>
        </div>
</div>
<!--图片裁剪框 end-->

<div class="fly-header layui-bg-black">
  <div class="layui-container">
    <a class="fly-logo" >
      <img src="static/images/logo.png"  height="43px"> 
    </a>
    <ul class="layui-nav fly-nav-user">
      <!-- 登入后的状态 -->
      <li class="layui-nav-item" id="replaceImg">
        <a class="fly-nav-avatar" href="javascript:;">
          <cite class="layui-hide-xs">${sessionScope.user.username} </cite>
          <img id="finalImg"  src="${sessionScope.user.avatar}">
        </a>
      </li>
       <li class="layui-nav-item">
        <a id="set"><i class="iconfont icon-shezhi">设置</i></a>
      </li>
      <li class="layui-nav-item">
        <a id="quit" ><i class="iconfont icon-tuichu"></i>退出</a>
      </li>
    </ul>
  </div>
</div>

</body>
<script type="text/javascript">
</script>
<script>
document.oncontextmenu=function(ev){
    return false;    //屏蔽右键菜单
}
layui.use(['jquery','layer','layim'], function(layim){
	var $=layui.jquery;
	var layer = layui.layer;
	var layim=layui.layim;
	var load1=layer.msg('连接服务器', {
		  icon: 16
		  ,shade: 0.01
		});

	var hostname = '${applicationScope.serverIp}';
	var clientid='';
	var port = 8083;

	var client = new Paho.MQTT.Client(hostname, port,clientid);

	var option={
		timeout:30,
		willMessage:willMessage(),
		userName:'${sessionScope.user.phone}',
		password:'${sessionScope.user.password}',
		cleanSession:true,
		onSuccess:onConnectSuccess,
		onFailure:onConnectFailure,
		reconnect:false,
		};

    client.onConnectionLost = onConnectionLost;
	client.onMessageDelivered = onMessageDelivered;
	client.onMessageArrived = onMessageArrived;
	client.connect(option);

	function willMessage() {
		var willMessage = new Paho.MQTT.Message(JSON.stringify({type:6,userid:'${sessionScope.user.id}',sessionid:'${sessionScope.sessionid}'}));
			willMessage.destinationName = "${applicationScope.serverTopic}";
			willMessage.qos=2;
		return willMessage;
	}

	function onConnectSuccess(res) {
		subscribe('${sessionScope.user.id}');
		layer.close(load1);
		var load2=layer.msg('获取好友列表', {
			  icon: 16
			  ,shade: 0.01
			});
		$.get("online",{'time':new Date().getTime()},function(res){
			if(res.code==0){
				layim.config({	
					init: {
						  url: 'friendList' 
						  ,type: 'get' //默认get，一般可不填
						  ,data: {} //额外参数
						}  ,
				    //获取群员接口
				    members: {
				      url: 'bevyList' //接口地址
				    }
				    
				    //上传图片接口，若不开启图片上传，剔除该项即可
				    ,uploadImage: {
				      url: 'uploadImage' //接口地址
				      ,type: 'post' //默认post
				    } 
				    
				    //上传文件接口，若不开启文件上传，剔除该项即可
				    ,uploadFile: {
				      url: 'uploadImage' //接口地址
				      ,type: 'post' //默认post
				    }
				    ,notice:true
				    ,maxLength:300
					,copyright:true
				    
				    ,msgbox: layui.cache.dir + 'css/modules/layim/html/msgbox.html' //消息盒子页面地址，若不开启，剔除该项即可
				    ,find: layui.cache.dir + 'css/modules/layim/html/find.html' //发现页面地址，若不开启，剔除该项即可
				    ,chatLog: layui.cache.dir + 'css/modules/layim/html/chatlog.html' //聊天记录页面地址，若不开启，剔除该项即可
				    ,createGroup: layui.cache.dir + 'css/modules/layim/html/createGroup.html' //创建群页面地址，若不开启，剔除该项即可
                    ,Information: layui.cache.dir + 'css/modules/layim/html/getInformation.html' //好友群资料页面
				  });
				var targetIndex = 0;
				//获取好友列表完毕,获取离线消息
			 	layim.on('ready', function(options){
			 		var targetObj;
			 		$(".layim-list-friend >li>h5").contextMenu({
		                width: 140, // width
		                itemHeight: 30, // 菜单项height
		                bgColor: "#fff", // 背景颜色
		                color: "#333", // 字体颜色
		                fontSize: 15, // 字体大小
		                hoverBgColor: "#009bdd", // hover背景颜色
		                hoverColor: "#fff", // hover背景颜色
		                target: function(ele) { // 当前元素
		                	targetObj = ele.parent();
		                	console.log(targetIndex);
		                	console.log(ele);
		                },
		                menu: [
		                	{ // 菜单项
		                        text: "新建分组",
		                        icon: "&#xe654",
		                        callback: function(ele) {
		                        	layer.prompt({title: '新建分组', formType: 0, maxlength: 7,shade :0,btnAlign: 'c'}, function(value, index){
		                        		$.get("newGroup",{"groupname":value},function(res){
		                        			if(res.code==0){
		                        				$('.layim-list-friend').append('<li><h5 layim-event="spread" lay-type="false" data-id="'+res.data.group.id+'"><i class="layui-icon">&#xe602;</i><span>'+res.data.group.groupname+'</span><em>(<cite class="layim-count"> 0</cite>)</em></h5><ul class="layui-layim-list"><li class="layim-null">该分组下暂无好友</li></ul></li>');
		                        				layer.msg("新建成功");
		                        			}else{
		                        				layer.msg("新建失败");
		                        			}
		                        			layer.close(index);
		                        		});
		                        	});
		                        }
		                    },
		                    { // 菜单项
		                        text: "修改分组",
		                        icon: "&#xe642",
		                        callback: function(ele) {
		                        	targetIndex = $('.layim-list-friend >li').index(targetObj);
		                        	layer.prompt({title: '新分组名称', formType: 0, maxlength: 7,shade :0,value:$(targetObj).find("span:first").text(),btnAlign: 'c'}, function(value, index){
		                        		$.get("updateGroup",{"groupname":value,"targetIndex":targetIndex},function(res){
		                        			if(res.code==0){
		                        				targetObj.find("span:first").text(value);
		                        				layer.msg("修改成功");
		                        			}else{
		                        				layer.msg("修改失败");
		                        			}
		                        			layer.close(index);
		                        		});
		                        	});
		                        }
		                    },
		                    {
		                        text: "删除分组",
		                        icon: "&#x1006",
		                        callback: function(ele) {
		                        	targetIndex = $('.layim-list-friend >li').index(targetObj);
		                        		$.get("deleteGroup",{"targetIndex":targetIndex},function(res){
		                        			if(res.code==0){
		                        				targetObj.remove();
		                        				layer.msg("删除成功");
		                        			}else{
		                        				layer.msg(res.msg);
		                        			}
		                        			layer.close(index);
		                        		});
		                        }
		                    }
		                ]
		            });
			 		
			            $(".layim-list-friend >li > ul >li").contextMenu({
			                width: 140, // width
			                itemHeight: 30, // 菜单项height
			                bgColor: "#fff", // 背景颜色
			                color: "#333", // 字体颜色
			                fontSize: 15, // 字体大小
			                hoverBgColor: "#009bdd", // hover背景颜色
			                hoverColor: "#fff", // hover背景颜色
			                target: function(ele) { // 当前元素
			                	targetObj = ele;
			                },
			                menu: [
			                    {
			                        text: "修改备注",
			                        icon: "&#xe642",
			                        callback: function(ele) {
			                        	   var uid = $.trim(targetObj.attr("class").replace(/^layim-friend/g, ''));
			                        	   layer.prompt({title: '好友备注', formType: 0, maxlength: 7,shade :0,value:$(targetObj).find("span:first").text(),btnAlign: 'c'}, function(value, index){
				                        		$.get("updateRemark",{"uid":uid,"remark":value},function(res){
				                        			if(res.code==0){
				                        				targetObj.find("span:first").text(value);
				                        				layer.msg("修改成功");
				                        			}else{
				                        				layer.msg("修改失败");
				                        			}
				                        			layer.close(index);
				                        		});
				                        	});
			                        }
			                    },
			                   /*  {
			                        text: "移动分组",
			                        icon: "&#xe604",
			                        callback: function(ele) {
			                            
			                        }
			                    }, */
			                    {
			                        text: "删除好友",
			                        icon: "&#x1006",
			                        callback: function(ele) {
			                        	var uid = $.trim(targetObj.attr("class").replace(/^layim-friend/g, ''));
			                        	$.get("deleteFriend",{"uid":uid},function(res){
			                        		 if(res.code==0){
			                        			 layim.removeList({
			                        				  type: 'friend' 
			                        				  ,id: uid 
			                        				});
			                        			 layer.msg("删除成功");
			                        		 }else{
			                        			 layer.msg("删除失败");
			                        		 }
			                        	});
			                        }
			                    }   
			                ]
			            });
			 		//群事件
			 		$(".layim-list-group > li").contextMenu({
		                width: 140, // width
		                itemHeight: 30, // 菜单项height
		                bgColor: "#fff", // 背景颜色
		                color: "#333", // 字体颜色
		                fontSize: 15, // 字体大小
		                hoverBgColor: "#009bdd", // hover背景颜色
		                hoverColor: "#fff", // hover背景颜色
		                target: function(ele) { // 当前元素
		                	targetObj = ele;
		                	console.log(ele);
		                },
		                menu: [
		                	{ // 菜单项
		                        text: "修改名片",
		                        icon: "&#xe642",
		                        callback: function(ele) {
		                        	var uid = $.trim(targetObj.attr("class").replace(/^layim-group/g, ''));
		                        	$.get("lookGroupRemark",{"uid":uid},function(res){
		                        		layer.prompt({title: '新的群名片', formType: 0, maxlength: 7,shade :0,value:res.data.bevymember.remark,btnAlign: 'c'}, function(value, index){
			                        		$.get("modifyBevyRemark",{"uid":uid,"remark":value},function(data){
			                        			if(res.code==0){
			                        				layer.msg("修改成功");
			                        			}else{
			                        				layer.msg("修改失败");
			                        			}
			                        			layer.close(index);
			                        		});
			                        	});
		                        	});	                        	 
		                        }
		                    },
		                	{ // 菜单项
		                        text: "退出该群",
		                        icon: "&#x1006",
		                        callback: function(ele) {
		                        	var uid = $.trim(targetObj.attr("class").replace(/^layim-group/g, ''));
		                        	$.get("exitGroup",{"uid":uid},function(res){
		                        		 if(res.code==0){
		                        			 layim.removeList({
		                        				  type: 'group' 
		                        				  ,id: uid 
		                        				});
		                        			 layer.msg("退出成功");
		                        		 }else{
		                        			 layer.msg(res.msg);
		                        		 }
		                        	});
		                        }
		                    }
		                ]
		            });
			 		layer.close(load2);
			 		var load3=layer.msg('获取离线消息', {
						  icon: 16
						  ,shade: 0.01
						});
						$.get("hideMessage",function(res){
							if(res.data.msgbox!=0){
								layim.msgbox(res.data.msgbox); 
							}
								$.each(res.data.data,function(index,value){
									layim.getMessage(value);
								});							
								layer.close(load3);
						});
					}); 
				
			}else{
				window.location.href = "index";
			}
		});
	}

	function onConnectFailure(error){
		console.log("断开连接！错误码："+error.errorCode +" 错误信息："+error.errorMessage);
		 window.location.href = "index";
	}

	function subscribe(topic){
		var option={
				qos:2,
				onSuccess:onSubscribeSuccess,
				onFailure:onSubscribeFailure,
				timeout:3
			};
		client.subscribe(topic,option);
	}

	function onSubscribeSuccess(){
		console.log("连接服务器成功");
	}

	function onSubscribeFailure(error){
		console.log("订阅失败"+error);
	}

	function sendMessage(topic,content){	
		var	message = new Paho.MQTT.Message(content);
			message.destinationName = topic;
			message.qos=2;
			message.retained=false;
			client.send(message);
	}
	
	function onConnectionLost(error) {
		console.log("连接断开:"+error.errorMessage);
		$.get("quit",function(res){
			window.location.href = "index";
		});
    }
        	
	function onMessageDelivered(message){
		console.log("发送成功！主题："+message.destinationName+" 内容："+message.payloadString +" ");
	}
        	
	function onMessageArrived(message) {
		var res = JSON.parse(message.payloadString);

		if(res.type==1){  
   		 layim.getMessage(res.data);  
   	 }
		
		if(res.data.type==5){
			 layim.setFriendStatus(res.data.user.id, 'online');
			layer.msg(res.data.user.username+"上线了");
		}
		
		if(res.data.type==6){
			layim.setFriendStatus(res.data.user.id, 'offline');
			layer.msg(res.data.user.username+"下线了");
		}
		
		if(res.data.type==7){
			if(res.data.msgbox!=0){
				layim.msgbox(res.data.msgbox); 
			}
		}
		//新添加的好友必须刷新才可以绑定右键事件
		if(res.data.type==9){
			layim.addList({
				       type: 'friend' 
					  ,avatar: res.data.user.avatar
					  ,username: res.data.user.username
					  ,groupid: res.data.groupid
					  ,id:res.data.user.id
					  ,sign: res.data.user.sign
			 });
			layer.msg("已同意您的好友申请");
		}
		//删除好友
		if(res.data.type==8){
			layim.removeList({
				  type: 'friend' //或者group
				  ,id: res.data.user.id //好友或者群组ID
			});
		}
		if(res.data.type==10){
			$.get("getBevyInfo",{"id":res.data.notify.groupid},function(data){
				console.log(data);
				layim.addList({
				       type: 'group' 
					  ,avatar: data.data.bevy.avatar
					  ,groupname: data.data.bevy.groupname
					  ,id:data.data.bevy.id
			 });
	      });
		}
	};

	layim.on('sendMessage', function(res){
		$.get("sendMessage",{"type":1,"message":JSON.stringify(res)},function(data){
			if(data.code!=0){
				layer.msg("发送失败");
			}else{
				$(".layim-chat-mine>.layim-chat-text").off( "contextmenu" ); 
				console.log($(".layim-chat-mine>.layim-chat-text").text());
				$(".layim-chat-mine>.layim-chat-text").contextMenu({
	                width: 140, // width
	                itemHeight: 30, // 菜单项height
	                bgColor: "#fff", // 背景颜色
	                color: "#333", // 字体颜色
	                fontSize: 15, // 字体大小
	                hoverBgColor: "#009bdd", // hover背景颜色
	                hoverColor: "#fff", // hover背景颜色
	                target: function(ele) { // 当前元素

	                },
	                menu: [
	                    {
	                        text: "删除",
	                        icon:"&#xe640",
	                        callback: function(ele) {
	                        	  layer.msg("隐藏功能，等待下个版本！");
	                        }
	                    },
	                    {
	                        text: "撤回",
	                        icon:"&#xe609",
	                        callback: function(ele) {
	                        	layer.msg("隐藏功能，等待下个版本！");
	                        }
	                    }   
	                ]
	            });
			}
		});
   });

	layim.on('sign', function(value){
		  console.log(value); //获得新的签名
		  $.get("modifySign",{"sign":value},function(res){
			
		  });
		  
		}); 
	
	//每次窗口打开或切换，即更新对方的状态
 	layim.on('chatChange', function(res){
 	  var type = res.data.type;
 	  if(type == 'friend'){
 		  //获取数据库该用户在线状态,本来使用浏览器缓存里获取的但是感觉不准确，还是请求一下后台吧
 		  $.get("getUserInfo",{id:res.data.id},function(data){
 			  if(data.code==0){
 				  if(data.data.user.sign==null){
 					 data.data.user.sign='';
 				  }
 		 		  if(data.data.user.status=="online"){
 		 			   layim.setChatStatus('<span style="color:#3FDD86;">[在线]</span>'+data.data.user.sign+''); //标注好友在线状态
 		 		  }else if(data.data.user.status=="hide"){
 		 			layim.setFriendStatus(data.data.user.id, 'offline');
 		 			 layim.setChatStatus('<span style="color:#FF5722;">[离线]</span>'+data.data.user.sign+'');
 		 		  }
 			  }else{
 				  layer.msg(data.msg);
 			  }
 		  });
 	  } else if(type == 'group'){
 	    //显示群有多少人
 		 $.get("getBevyInfo",{id:res.data.id},function(data){
			  if(data.code==0){
				  if(data.data.bevy.des==null){
					  data.data.bevy.des="";
				  }
				  layim.setChatStatus('<span>['+data.data.bevy.id+'] '+data.data.bevy.des+'</span>'); 
			  }
		  });
 	  }
 	});
	
	
	//点击退出事件
	$("#quit").click(function(){		
	 	layer.open({
	   	   title:'提示'
	   	  ,btn: ['确定','取消'] //按钮
	   	  ,content: '<div style="padding: 1px 80px;">确定退出吗？</div>'
	   	  ,btnAlign: 'c' //按钮居中
	   	 ,closeBtn: 0  //不显示右上角关闭
	   	 ,yes: function(){
	   		$.get("quit",function(res){
				window.location.href = "index";
			});
	  	 	}
	   	,cancle: function(index){
	   		layer.close(index);
	 	  }
	   	});
	});
});
</script>
<script>
    //弹出框水平垂直居中
    (window.onresize = function () {
        var win_height = $(window).height();
        var win_width = $(window).width();
        if (win_width <= 768){
            $(".tailoring-content").css({
                "top": (win_height - $(".tailoring-content").outerHeight())/2,
                "left": 0
            });
        }else{
            $(".tailoring-content").css({
                "top": (win_height - $(".tailoring-content").outerHeight())/2,
                "left": (win_width - $(".tailoring-content").outerWidth())/2
            });
        }
    })();

    //弹出图片裁剪框
    $("#replaceImg").on("click",function () {
        $(".tailoring-container").toggle();
    });
    //图像上传
    function selectImg(file) {
        if (!file.files || !file.files[0]){
            return;
        }
        var reader = new FileReader();
        reader.onload = function (evt) {
            var replaceSrc = evt.target.result;
            //更换cropper的图片
            $('#tailoringImg').cropper('replace', replaceSrc,false);//默认false，适应高度，不失真
        }
        reader.readAsDataURL(file.files[0]);
    }
    //cropper图片裁剪
    $('#tailoringImg').cropper({
        aspectRatio: 1/1,//默认比例 
        preview: '.previewImg',//预览视图
        guides: false,  //裁剪框的虚线(九宫格)
        autoCropArea: 0.5,  //0-1之间的数值，定义自动剪裁区域的大小，默认0.8
        movable: false, //是否允许移动图片
        dragCrop: true,  //是否允许移除当前的剪裁框，并通过拖动来新建一个剪裁框区域
        movable: true,  //是否允许移动剪裁框
        resizable: true,  //是否允许改变裁剪框的大小
        zoomable: true,  //是否允许缩放图片大小
        mouseWheelZoom: true,  //是否允许通过鼠标滚轮来缩放图片
        touchDragZoom: true,  //是否允许通过触摸移动来缩放图片
        rotatable: true,  //是否允许旋转图片
        crop: function(e) {
            // 输出结果数据裁剪图像。
        }
    });
    //旋转
    $(".cropper-rotate-btn").on("click",function () {
        $('#tailoringImg').cropper("rotate", 45);
    });
    //复位
    $(".cropper-reset-btn").on("click",function () {
        $('#tailoringImg').cropper("reset");
    });
    //换向
    var flagX = true;
    $(".cropper-scaleX-btn").on("click",function () {
        if(flagX){
            $('#tailoringImg').cropper("scaleX", -1);
            flagX = false;
        }else{
            $('#tailoringImg').cropper("scaleX", 1);
            flagX = true;
        }
        flagX != flagX;
    });

    //裁剪后的处理
    $("#sureCut").on("click",function () {
        if ($("#tailoringImg").attr("src") == null ){
            return false;
        }else{
            var cas = $('#tailoringImg').cropper('getCroppedCanvas');//获取被裁剪后的canvas
            var base64url = cas.toDataURL('image/png'); //转换为base64地址形式
            $.post("GenerateImage",{"finalimg":base64url},function(res){
            	if(res.code==0){
            		$("#finalImg").prop("src",base64url);//显示为图片的形式
            	}
            })  
            //关闭裁剪框
            closeTailor();
        }
    });
    
    //关闭裁剪框
    function closeTailor() {
        $(".tailoring-container").toggle();
    }
</script>
<script type="text/javascript">
//监听新建群组按钮
$(".set").click(function(){
	  parent.layui.layer.open({
		  type: 2,
		  title: '用户信息',
		  closeBtn: 1, //不显示关闭按钮
		  fixed: false,
		  maxmin:true,
		  shade:0,
		  offset: 'auto',
		  area: ['370px', '350px'],
		  content: ['static/css/modules/layim/html/getInformation.html?', 'no'], //iframe的url，no代表不显示滚动条
		  end: function(){ //此处用于演示
				
		  }
		});
	  return false;
});
</script>
</html>

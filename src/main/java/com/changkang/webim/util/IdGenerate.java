package com.changkang.webim.util;

import java.security.SecureRandom;
import java.util.UUID;


/**
 * 封装各种生成唯一性ID算法的工具类.
 */
public class IdGenerate  {

	private static SecureRandom random = new SecureRandom();
	
	/**
	 * 封装JDK自带的UUID, 通过Random数字生成, 中间无-分割.
	 */
	public static String uuid() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
	/**
	 * 使用SecureRandom随机生成Long. 
	 */
	public static long randomLong() {
		return Math.abs(random.nextInt(999999));
	}
	/**
	 * 使用SecureRandom随机生成Long.
	 */
	public static Integer randomInt( ) {
		Integer a =Math.abs(random.nextInt(999999));
		if(a<900000){
			a=randomInt();
		}
		return a;
	}


	
	public static void main(String[] args) {
		System.out.println(IdGenerate.uuid());
		System.out.println(IdGenerate.uuid().length());
	}

}

package com.changkang.webim.model;


import java.util.List;

import com.changkang.webim.model.base.BaseGroup;
import com.jfinal.plugin.activerecord.Db;


@SuppressWarnings("serial")
public class Group extends BaseGroup<Group> {
		
public static final Group dao = new Group().dao();
	
private List<User> list;
	
	
	public List<User> getList() {
		return list;
	}

	public void setList(List<User> list) {
		this.list = list;
	}

	
	/**
	 * 查找所有分组
	 * @return
	 */
	public List<Group> groups(String id){
		return dao.find(Db.getSql("user.group"),id);
	}
}

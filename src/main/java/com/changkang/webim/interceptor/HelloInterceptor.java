package com.changkang.webim.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
/**
 * 拦截器
 * @author Administrator
 *
 */
public class HelloInterceptor implements  Interceptor{

	@Override
	public void intercept(Invocation inv) {
		inv.invoke();
	}

}

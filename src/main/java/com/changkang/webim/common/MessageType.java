package com.changkang.webim.common;

/**
 * 枚举用户消息类型
 * @author 常康
 *
 */
public enum MessageType {
	
	online ("5","在线"), 
	
	hide ("6","离线");
	
	
	
	private String type;  //值
    private String description;  //描述
      
    MessageType(String type, String description) {  
        this.type = type;  
        this.description = description;  
    }  
      
    public String type() {  
        return this.type;  
    }  
      
    public String description() {  
        return this.description;  
    }  

}

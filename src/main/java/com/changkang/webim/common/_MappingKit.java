package com.changkang.webim.common;


import com.changkang.webim.model.Bevy;
import com.changkang.webim.model.Bevymember;
import com.changkang.webim.model.Chat;
import com.changkang.webim.model.Friend;
import com.changkang.webim.model.Group;
import com.changkang.webim.model.Notify;
import com.changkang.webim.model.User;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

public class _MappingKit {
	
	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("bevy", "id", Bevy.class);
		arp.addMapping("bevymember", "id", Bevymember.class);
		arp.addMapping("chat", "id", Chat.class);
		arp.addMapping("friend", "id", Friend.class);
		arp.addMapping("group", "id", Group.class);
		arp.addMapping("notify", "id", Notify.class);
		arp.addMapping("user", "id", User.class);
	}
}


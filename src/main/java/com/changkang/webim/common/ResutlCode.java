package com.changkang.webim.common;

/**
 * 枚举全局返回码
 * @author 常康
 *
 */
public enum ResutlCode {
	/**0请求成功*/
	SUCCESS (0,"请求成功"), 	
	/**4权限不足*/
	DENY(4,"权限不足"),
	/**未登录*/
	NOTLOGGED(1,"未登录"),
	/**3请求失败*/
	FAIL(3,"请求失败"),
	/**404未发现资源*/
	NOTFOUND(404,"请求资源不存在"),
	/**500服务器发生异常*/
	SERVERERROR(500,"服务器发生异常");
	
	/**返回码*/
	private int code;  
	/**返回码描述备注*/
    private String description;  
      
    ResutlCode(int code, String description) {  
        this.code = code;  
        this.description = description;  
    }  
      
    public int code() {  
        return this.code;  
    }  
      
    public String description() {  
        return this.description;  
    }  
}

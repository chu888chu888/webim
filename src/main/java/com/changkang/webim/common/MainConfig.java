package com.changkang.webim.common;



import com.changkang.webim.handler.HelloHandler;
import com.changkang.webim.interceptor.HelloInterceptor;
import com.changkang.webim.mqtt.MqttPlugin;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.UrlSkipHandler;
import com.jfinal.json.MixedJsonFactory;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
/**
 * 框架主要配置类
 * @author 常康
 *
 */
public class MainConfig extends JFinalConfig{
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		// 加载少量必要配置
		PropKit.use("a_little_config.txt");
		me.setViewType(ViewType.JSP);
		me.setDevMode(PropKit.getBoolean("devMode", PropKit.getBoolean("jfinal.devMode")));
		//设置全局错误返回
		me.setRenderFactory(new HelloRenderFactory());
		me.setBaseUploadPath("upload");// 配置文件上传路径
		me.setJsonFactory(new MixedJsonFactory());

	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		//添加一个路由
		me.add(new _RouteKit());
	}
	
	/**
	 * 配置模板引擎
	 */
	public void configEngine(Engine me) {

	}
	
	public static DruidPlugin createDruidPlugin() {
		return new DruidPlugin(PropKit.get("jdbc.url"), PropKit.get("jdbc.user"), PropKit.get("jdbc.password").trim());
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		// 配置C3p0数据库连接池插件
		DruidPlugin druidPlugin = createDruidPlugin();
		me.add(druidPlugin);
		//配置自定义的mqtt消息队列插件
		MqttPlugin mqttPlugin=new MqttPlugin();
		me.add(mqttPlugin);
		// 配置ActiveRecord插件,并指定configName为 mysql
		ActiveRecordPlugin arp = new ActiveRecordPlugin("mysql",druidPlugin);
		//配置sql语句路径
		arp.setBaseSqlTemplatePath(PathKit.getRootClassPath()+"/sql");
		//添加slq语句文件
		arp.addSqlTemplate("sql.txt");
		//打印sql语句
		arp.setShowSql(true);
		//添加数据库和model对象映射
	    _MappingKit.mapping(arp);
		// 所有映射在 MappingKit 中自动化搞定
		me.add(arp);
	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		//对所有aciton请求拦截
		me.addGlobalActionInterceptor(new HelloInterceptor());
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		/**不处理websocket请求*/
		me.add(new UrlSkipHandler("^/websocket", false));
		/**对所有访问过滤处理*/
		me.add(new HelloHandler());
	}
	/**
	 * 项目关闭前执行
	 */
	@Override
	public void beforeJFinalStop() {
		LogKit.info("项目准备结束");
	}
	/**
	 * 项目启动后执行
	 */
	@Override
	public void afterJFinalStart() {
		LogKit.info("项目启动成功");
	}
}

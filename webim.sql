/*
Navicat MySQL Data Transfer

Source Server         : aliyun
Source Server Version : 50716
Source Host           : www.studentweb.cn:3306
Source Database       : webim

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2018-07-16 19:27:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bevy
-- ----------------------------
DROP TABLE IF EXISTS `bevy`;
CREATE TABLE `bevy` (
  `id` varchar(255) NOT NULL COMMENT '群的topic',
  `groupname` varchar(255) DEFAULT NULL COMMENT '群名称',
  `belong` varchar(255) DEFAULT NULL COMMENT '群主',
  `avatar` varchar(255) DEFAULT NULL COMMENT '群头像',
  `des` varchar(255) DEFAULT NULL COMMENT '群描述',
  `status` varchar(255) DEFAULT NULL COMMENT '群状态 是否禁言',
  `number` int(11) DEFAULT NULL COMMENT '人数',
  `timestamp` bigint(255) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bevymember
-- ----------------------------
DROP TABLE IF EXISTS `bevymember`;
CREATE TABLE `bevymember` (
  `id` varchar(255) NOT NULL,
  `bevyid` varchar(255) DEFAULT NULL COMMENT '群id',
  `useid` varchar(255) DEFAULT NULL COMMENT '群成员id',
  `remark` varchar(255) DEFAULT NULL COMMENT '成员备注',
  `timestamp` bigint(255) DEFAULT NULL COMMENT '进群时间',
  `status` int(255) DEFAULT NULL COMMENT '1：正常',
  `type` int(11) DEFAULT NULL COMMENT '1 群主  2 管理员 0 普通',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for chat
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat` (
  `id` varchar(255) NOT NULL,
  `fromid` varchar(255) DEFAULT NULL,
  `toid` varchar(255) DEFAULT NULL,
  `content` text,
  `timestamp` bigint(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for friend
-- ----------------------------
DROP TABLE IF EXISTS `friend`;
CREATE TABLE `friend` (
  `id` varchar(255) NOT NULL,
  `groupid` varchar(255) DEFAULT NULL COMMENT '分组id',
  `userid` varchar(255) DEFAULT NULL COMMENT '用户id',
  `remark` varchar(255) DEFAULT NULL COMMENT '好友备注',
  `timestamp` bigint(255) DEFAULT NULL,
  `belong` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `id` varchar(255) NOT NULL,
  `groupname` varchar(255) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for mqttacl
-- ----------------------------
DROP TABLE IF EXISTS `mqttacl`;
CREATE TABLE `mqttacl` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `allow` int(1) DEFAULT NULL COMMENT '0: deny, 1: allow',
  `ipaddr` varchar(60) DEFAULT NULL COMMENT 'IpAddress',
  `username` varchar(100) DEFAULT NULL COMMENT 'Username',
  `clientid` varchar(100) DEFAULT NULL COMMENT 'ClientId',
  `access` int(2) NOT NULL COMMENT '1: subscribe, 2: publish, 3: pubsub',
  `topic` varchar(100) NOT NULL DEFAULT '' COMMENT 'Topic Filter',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for notify
-- ----------------------------
DROP TABLE IF EXISTS `notify`;
CREATE TABLE `notify` (
  `id` varchar(255) NOT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1为请求添加用户2为系统消息（添加好友）3为请求加群 4为系统消息（添加群） 5 全体会员消息''',
  `from` varchar(255) DEFAULT NULL COMMENT '''消息发送者 0表示为系统消息''',
  `to` varchar(255) DEFAULT NULL COMMENT '''消息接收者 0表示全体会员'',',
  `status` int(11) DEFAULT NULL COMMENT '''1未处理 2同意 3拒绝 4同意且返回消息已读 5拒绝且返回消息已读 6全体消息已读'',',
  `remark` varchar(255) DEFAULT NULL COMMENT '''附加消息'',',
  `timestamp` varchar(255) DEFAULT NULL COMMENT '''发送消息时间'',',
  `handlerid` varchar(255) DEFAULT NULL COMMENT '处理该请求的管理员id',
  `groupid` varchar(255) DEFAULT NULL COMMENT '好友分组',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL COMMENT '手机就是账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `username` varchar(255) DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `sign` varchar(255) DEFAULT NULL COMMENT '签名',
  `status` varchar(255) DEFAULT NULL,
  `gender` int(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `qq` varchar(255) DEFAULT NULL,
  `wechat` varchar(255) DEFAULT NULL,
  `regtime` datetime DEFAULT NULL,
  `is_superuser` varchar(255) DEFAULT NULL,
  `logintime` datetime DEFAULT NULL,
  `quittime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
